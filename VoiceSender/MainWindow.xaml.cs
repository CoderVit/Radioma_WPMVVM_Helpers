﻿using NAudio.Wave;
using System;
using System.Windows;

namespace VoiceSender
{
    public partial class MainWindow
    {
        private WaveIn m_waveIn;
        private WaveOut m_waveOut;
        private BufferedWaveProvider m_waveProvider;
        private short[] m_playBuffer;
        private int m_bufferPointer;

        public MainWindow()
        {
            InitializeComponent();
            m_playBuffer = new short[1249];
            m_bufferPointer = 0;
        }

        private void StartVoice_OnClick(object sender, RoutedEventArgs e)
        {
            // Setup Incoming
            m_waveIn = new WaveIn
            {
                DeviceNumber = 0,
                WaveFormat = new WaveFormat(8000, 16, 1),
                BufferMilliseconds = 13
            };

            m_waveIn.DataAvailable += SourceStreamOnDataAvailable;
            m_waveIn.StartRecording();

            // Setup Output
            m_waveOut = new WaveOut();
            m_waveProvider = new BufferedWaveProvider(new WaveFormat(8000, 16, 1));

            m_waveOut.Init(m_waveProvider);
            m_waveOut.Play();

            StartVoice.Visibility = Visibility.Collapsed;
            EndVoice.Visibility = Visibility.Visible;
        }

        private void SourceStreamOnDataAvailable(object sender, WaveInEventArgs e)
        {
            if (m_waveProvider == null) return;

            //var linearArray = Convertor.AlawToLinear(e.Buffer);
            //var allowArray = Convertor.LinearToAllow(linearArray);
            m_waveProvider.AddSamples(e.Buffer, 0, e.Buffer.Length);
        }

        private void EndVoice_OnClick(object sender, RoutedEventArgs e)
        {
            if (m_waveIn != null)
            {
                m_waveIn.StopRecording();
                m_waveIn.Dispose();
                m_waveIn = null;
            }

            if (m_waveOut != null)
            {
                m_waveOut.Stop();
                m_waveOut.Dispose();
                m_waveOut = null;
            }

            StartVoice.Visibility = Visibility.Visible;
            EndVoice.Visibility = Visibility.Collapsed;
        }
    }

    internal static class Convertor
    {
        private const byte SIGN_BIT = 0x80;     /* Sign bit for a A-law byte. */
        private const int QUANT_MASK = 0xF;     /* Quantization field mask. */
        private const int SEG_SHIFT = 4;        /* Left shift for segment number. */
        private const int SEG_MASK = 0x70;      /* Segment field mask. */

        public static short[] AlawToLinear(byte[] waveBuffer)
        {
            var linearArray = new short[waveBuffer.Length];

            for (var bufferIterator = 0; bufferIterator < waveBuffer.Length; bufferIterator++)
            {
                var alaw = waveBuffer[bufferIterator] ^ 0x55;

                Int16 t = Convert.ToInt16((alaw & QUANT_MASK) << 4);

                Int16 segment = Convert.ToInt16((alaw & SEG_MASK) >> SEG_SHIFT);

                switch (segment)
                {
                    case 0:
                        t += 8;
                        break;
                    case 1:
                        t += 0x108;
                        break;
                    default:
                        t += 0x108;
                        t <<= segment - 1;
                        break;
                }

                linearArray[bufferIterator] = Convert.ToInt16((alaw & SIGN_BIT) >= 128 ? t : -t);
            }
            return linearArray;
        }

        public static byte[] LinearToAllow(short[] pcmValues)
        {
            var allows = new byte[pcmValues.Length];

            for (var pcmIterator = 0; pcmIterator < pcmValues.Length; pcmIterator++)
            {
                var pcmValue = pcmValues[pcmIterator];
                short mask;

                pcmValue = (short)(pcmValue >> 3);

                if (pcmValue >= 0)
                {
                    mask = 0xD5;    /* sign (7th) bit = 1 */
                }
                else
                {
                    mask = 0x55;    /* sign bit = 0 */
                    pcmValue = Convert.ToInt16(-pcmValue - 1);
                }

                /* Convert the scaled magnitude to segment number. */
                var seg = Search(pcmValue);

                /* Combine the sign, segment, and quantization bits. */
                if (seg >= 8)       /* out of range, return maximum value. */
                {
                    allows[pcmIterator] = (byte)(0x7F ^ mask);
                }
                else
                {
                    var aval = (byte)(seg << SEG_SHIFT);
                    if (seg < 2)
                        aval |= (byte)((pcmValue >> 1) & QUANT_MASK);
                    else
                        aval |= (byte)((pcmValue >> seg) & QUANT_MASK);
                    allows[pcmIterator] = (byte)(aval ^ mask);
                }
            }

            return allows;
        }

        private static short Search(short val)
        {
            const short TABLE_SIZE = 8;
            short[] table = { 0x1F, 0x3F, 0x7F, 0xFF, 0x1FF, 0x3FF, 0x7FF, 0xFFF };

            for (short i = 0; i < TABLE_SIZE; i++)
            {
                if (val <= table[i])
                {
                    return i;
                }
            }
            return TABLE_SIZE;
        }
    }
}
