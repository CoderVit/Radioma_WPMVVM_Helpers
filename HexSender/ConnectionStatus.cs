﻿namespace UdpSender
{
    internal enum ConnectionStatus
    {
        CONNECTION_STATUS_DISCONNECTED = 0,
        CONNECTION_STATUS_CONNECTING,
        CONNECTION_STATUS_CONNECTED
    }
}
