﻿using System;
using System.Linq;
using System.Text;

namespace UdpSender
{
    public class HexConvertor
    {
        public byte[] SymbolToHex(string data)
        {
            return Enumerable
            .Range(0, data.Length)
            .Where(x => x % 2 == 0)
            .Select(x => Convert.ToByte(data.Substring(x, 2), 16))
            .ToArray();

        }

        public byte[] SymbolToByte(string data)
        {
            var bytes = Encoding.UTF8.GetBytes(data).ToList();
            bytes.Insert(0, 0x002);
            bytes.Add(0x003);
            return bytes.ToArray();
        }

        public string ToText(byte[] data)
        {
            return BitConverter.ToString(data);
        }
    }
}