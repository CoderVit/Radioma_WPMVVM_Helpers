﻿using System;
using System.Windows.Input;

namespace UdpSender.Commands
{
    public class RelayCommand : ICommand
    {
        private readonly Predicate<object> m_canExecute;
        private readonly Action<object> m_function;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        private RelayCommand(Action<object> asyncExecute, Predicate<object> canExecute)
        {
            m_function = asyncExecute;
            m_canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            if (m_canExecute == null)
            {
                return true;
            }

            return m_canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            m_function(parameter);
        }
    }
}