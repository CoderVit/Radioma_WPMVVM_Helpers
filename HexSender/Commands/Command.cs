﻿using System;
using System.Windows.Input;

namespace UdpSender.Commands
{
    internal class SimpleCommand : ICommand
    {
        private readonly Action m_execute;
        private readonly Predicate<object> m_canExecute;

        public SimpleCommand(Action execute)
            : this(execute, null) { }

        public SimpleCommand(Action execute, Predicate<object> canExecute)
        {
            m_execute = execute;
            m_canExecute = canExecute;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return m_canExecute == null || m_canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            m_execute();
        }
    }
}