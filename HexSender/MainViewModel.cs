﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using UdpSender.Commands;

namespace UdpSender
{
    internal class MainViewModel : INotifyPropertyChanged
    {
        private UdpClient m_controlUdpClient;
        private UdpClient m_commandUdpClient;
        private UdpClient m_serverUdpClient;

        private readonly HexConvertor m_convertor;

        public MainViewModel()
        {
            m_convertor = new HexConvertor();
            RemoteIp = "192.168.0.11";
            LocalRxTxPort = "60003";
            LocalTxPort = "60004";
            LocalRxPort = "60005";
        }

        private void Send(object control)
        {
            var textBox = control as TextBox;
            if (textBox == null || string.IsNullOrEmpty(textBox.Text)) return;
            if (SelectedType == 1)
            {
                var isCorrect = textBox.Text.Split('.').Skip(1).Any(t => t.Length != 2);
                if (!isCorrect)
                {
                    MessageBox.Show("Incorrect format");
                    return;
                }
            }
            var data = SendDataConvert(textBox.Text);
            if (data == null) return;
            m_commandUdpClient.Send(data, data.Length);
        }
        private void Clear(object control)
        {
            var textBox = control as TextBox;
            if (textBox == null) return;
            textBox.Text = "";
        }
        private void Toggle(object control)
        {
            var toggleButton = control as ToggleButton;
            if (toggleButton == null || toggleButton.IsChecked == null) return;
            (ConnectionStatus != ConnectionStatus.CONNECTION_STATUS_CONNECTED ? (Action)Connect : Disconnect)();
        }

        private void Connect()
        {
            IPAddress remoteIp;
            if (!IPAddress.TryParse(RemoteIp, out remoteIp))
            {
                MessageBox.Show("Incorrect IP");
                return;
            }
            ushort controlPort;
            if (!ushort.TryParse(LocalRxTxPort, out controlPort))
            {
                MessageBox.Show("Incorrect control port");
                return;
            }

            m_controlUdpClient = new UdpClient(controlPort)
            {
                Client =
                {
                    ReceiveTimeout = 50
                }
            };
            ushort remoteListenPort;
            try
            {
                m_controlUdpClient.Connect(remoteIp, controlPort);
                ConnectionStatus = ConnectionStatus.CONNECTION_STATUS_CONNECTING;
                var requestText = Encoding.UTF8.GetBytes("SERIAL:" + ushort.Parse(LocalRxPort));
                m_controlUdpClient.Send(requestText, requestText.Length);
                var endPoint = new IPEndPoint(IPAddress.Any, 0);
                var response = Encoding.UTF8.GetString(m_controlUdpClient.Receive(ref endPoint)).Split(':');
                if (response.Length != 2 || !response[0].Contains("OK")) throw new Exception("Host is busy");
                remoteListenPort = ushort.Parse(response[1]);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                m_controlUdpClient.Close();
                m_controlUdpClient = null;
                ConnectionStatus = ConnectionStatus.CONNECTION_STATUS_DISCONNECTED;
                return;
            }

            ushort localTxPort;
            if (!ushort.TryParse(LocalTxPort, out localTxPort))
            {
                MessageBox.Show("Incorrect remote Tx port");
                return;
            }
            ushort localRxPort;
            if (!ushort.TryParse(LocalRxPort, out localRxPort))
            {
                MessageBox.Show("Incorrect remote Rx port");
                return;
            }
            m_serverUdpClient = new UdpClient(localRxPort);
            try
            {
                m_commandUdpClient = new UdpClient
                {
                    Client =
                    {
                        ReceiveTimeout = 50
                    }
                };
                m_commandUdpClient.Connect(remoteIp, remoteListenPort);
                ConnectionStatus = ConnectionStatus.CONNECTION_STATUS_CONNECTED;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
                UdpStop();
                return;
            }
            UdpStart();
        }
        private void Disconnect()
        {
            var requestText = Encoding.UTF8.GetBytes("SERIAL:0");
            m_controlUdpClient.Send(requestText, requestText.Length);
            var endPoint = new IPEndPoint(IPAddress.Any, 0);
            try
            {
                if (Encoding.UTF8.GetString(m_controlUdpClient.Receive(ref endPoint)) != "OK")
                {
                    MessageBox.Show("Ошибка разрыва");
                    return;
                }
            }
            catch { }
            UdpStop();
        }

        private void UdpStart()
        {
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                var failPing = 0;
                var requestText = Encoding.UTF8.GetBytes("SERIAL:" + ushort.Parse(LocalRxPort));
                while (ConnectionStatus == ConnectionStatus.CONNECTION_STATUS_CONNECTED)
                {
                    try
                    {
                        m_controlUdpClient.Send(requestText, requestText.Length);
                        var endPoint = new IPEndPoint(IPAddress.Any, 0);
                        if (Encoding.UTF8.GetString(m_controlUdpClient.Receive(ref endPoint)).Contains("OK:"))
                        {
                            if (failPing > 0) failPing = 0;
                        }
                        else if (++failPing == 3) UdpStop();
                    }
                    catch
                    {
                        if (++failPing == 3) UdpStop();
                    }
                    finally
                    {
                        Thread.Sleep(3000);
                    }
                }
            }).Start();

            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                while (ConnectionStatus == ConnectionStatus.CONNECTION_STATUS_CONNECTED)
                {
                    var remoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
                    try
                    {
                        //variable, cuz one line created a closure
                        var data = ReceiveDataConvert(m_serverUdpClient.Receive(ref remoteIpEndPoint));
                        Log += "--> " + data + Environment.NewLine;
                    }
                    catch { }
                }
            }).Start();
        }
        private void UdpStop()
        {
            ConnectionStatus = ConnectionStatus.CONNECTION_STATUS_DISCONNECTED;
            if (m_controlUdpClient != null)
            {
                m_controlUdpClient.Close();
                m_controlUdpClient = null;
            }

            if (m_commandUdpClient != null)
            {
                m_commandUdpClient.Close();
                m_commandUdpClient = null;
            }

            if (m_serverUdpClient != null)
            {
                m_serverUdpClient.Close();
                m_serverUdpClient = null;
            }
        }

        private byte[] SendDataConvert(string text)
        {
            switch (SelectedType)
            {
                case 0:
                    return Encoding.UTF8.GetBytes(text);
                case 1:
                    return m_convertor.SymbolToHex(text.Replace(".", ""));
                case 2:
                    return m_convertor.SymbolToByte(text);
                default:
                    return null;
            }
        }
        private string ReceiveDataConvert(byte[] data)
        {
            switch (SelectedType)
            {
                case 0:
                case 2:
                    return Encoding.UTF8.GetString(data);
                case 1:
                    return m_convertor.ToText(data);
                default:
                    return null;
            }
        }

        #region Commands
        public ICommand SendCommand { get { return new RelayCommand(Send); } }
        public ICommand ClearCommand { get { return new RelayCommand(Clear); } }
        public ICommand ToggleCommand { get { return new RelayCommand(Toggle); } }
        public ICommand ClearLogCommand { get { return new SimpleCommand(() => Log = null); } }
        #endregion
        #region Properties
        private ConnectionStatus m_connectionStatus;
        public ConnectionStatus ConnectionStatus
        {
            get { return m_connectionStatus; }
            set
            {
                if (m_connectionStatus == value) return;
                m_connectionStatus = value;
                RaisePropertyChanged("ConnectionStatus");
            }
        }

        private int m_selectedType;
        public int SelectedType
        {
            get { return m_selectedType; }
            set
            {
                m_selectedType = value;
                RaisePropertyChanged("SelectedType");
            }
        }

        private volatile string m_log;
        public string Log
        {
            get { return m_log; }
            set
            {
                m_log = value;
                RaisePropertyChanged("Log");
            }
        }

        private string m_remoteIp;
        public string RemoteIp
        {
            get { return m_remoteIp; }
            set
            {
                m_remoteIp = value;
                RaisePropertyChanged("RemoteIp");
            }
        }

        private string m_localRxTxPort;
        public string LocalRxTxPort
        {
            get { return m_localRxTxPort; }
            set
            {
                m_localRxTxPort = value;
                RaisePropertyChanged("LocalRxTxPort");
            }
        }

        private string m_localTxPort;
        public string LocalTxPort
        {
            get { return m_localTxPort; }
            set
            {
                m_localTxPort = value;
                RaisePropertyChanged("LocalTxPort");
            }
        }

        private string m_localRxPort;
        public string LocalRxPort
        {
            get { return m_localRxPort; }
            set
            {
                m_localRxPort = value;
                RaisePropertyChanged("LocalRxPort");
            }
        }
        #endregion
        #region MVVM
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}