﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace Udp
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            AddLog("***---UDP log start---***");
        }

        private void SendBtn_OnClick(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(CommandTxtBx.Text)) return;
            IPAddress ipAddress;
            IPAddress.TryParse(RemoteIpTxtBx.Text, out ipAddress);
            if (ipAddress == null)
            {
                MessageBox.Show("Remote ip address incorrect or empty");
                return;
            }
            ushort remotePort;
            if (!ushort.TryParse(RemotePortTxtBx.Text, out remotePort))
            {
                MessageBox.Show("Remote port incorrect value");
                return;
            }
            ushort localPort;
            if (!ushort.TryParse(LocalPortTxtBx.Text, out localPort))
            {
                MessageBox.Show("Local port incorrect value");
                return;
            }
            SendBtn.IsEnabled = false;
            try
            {
                using (var client = new UdpClient(localPort))
                {
                    client.Client.SendTimeout = 5000;
                    client.Client.ReceiveTimeout = 5000;

                    AddLog("Sent: " + CommandTxtBx.Text);
                    var data = Encoding.UTF8.GetBytes(CommandTxtBx.Text);
                    client.Send(data, data.Length, new IPEndPoint(ipAddress, remotePort));

                    var remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
                    var rcvPacket = client.Receive(ref remoteEndPoint);
                    AddLog("Received from: " + remoteEndPoint + " - " + Encoding.ASCII.GetString(rcvPacket, 0, rcvPacket.Length));
                }
            }
            catch (Exception exception)
            {
                AddLog("Sent exception: " + exception.Message);
            }
            finally
            {
                SendBtn.IsEnabled = true;
            }
        }

        private void AddLog(string text)
        {
            Dispatcher.Invoke(() =>
            {
                LogTxtBox.Text += text + Environment.NewLine;
                LogTxtBox.Focus();
                LogTxtBox.SelectionStart = LogTxtBox.Text.Length;
            });
        }

        private void CommandTxtBx_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            SendBtn_OnClick(null, null);
        }

        private void LogClear_OnClick(object sender, RoutedEventArgs e)
        {
            LogTxtBox.Text = "";
        }
    }
}
