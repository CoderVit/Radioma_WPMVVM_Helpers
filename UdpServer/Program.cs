﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace UdpServer
{
    class Program
    {
        static void Main()
        {
            // Создаем UdpClient для чтения входящих данных
            var server = new UdpClient(2554);
            IPEndPoint remoteIpEndPoint = null;

            try
            {
                Console.WriteLine("\n-----------*******Server has been started*******-----------");

                while (true)
                {
                    // Ожидание дейтаграммы
                    var receiveBytes = server.Receive(ref remoteIpEndPoint);

                    // Преобразуем и отображаем данные
                    Console.WriteLine(" --> " + Encoding.UTF8.GetString(receiveBytes));
                    server.SendAsync(receiveBytes, receiveBytes.Length, remoteIpEndPoint);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Возникло исключение: " + ex + Environment.NewLine + ex.Message);
                Console.ReadLine();
            }
        }
    }
}
